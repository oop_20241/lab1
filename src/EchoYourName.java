import java.util.*;

public class EchoYourName {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Please input your name: ");
        String name = input.nextLine();
        System.out.println("Hello :" + name);
        input.close();

    }
}
